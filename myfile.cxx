#include <iostream>
#include <fstream>

int main(int argc, char** argv){
    std::ofstream myfile;
    std::cout<<"Hello world. Writting message to "<< argv[1] << std::endl;
    myfile.open(argv[1]);
    myfile<<" Writing this to this file. The message : "<<argv[2]<<std::endl;
    myfile.close();
    
    return 0;
}